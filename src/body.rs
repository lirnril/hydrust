//use std::pin::Pin;
//use std::task::{Context, Poll};
//use std::future::Future;
//use std::stream::Stream;

use hyper::body::Bytes;

use sync_promise::*;

type ChunkPromise = SyncPromise<Option<Box<Chunk>>>;

#[derive(Debug, Default)]
pub struct Chunk {
    buf: Bytes,
    next: ChunkPromise,
}

#[derive(Debug, Default)]
pub struct Body {
    head: ChunkPromise,
}

impl Body {
    #[inline]
    pub fn chunks(&self) -> Chunks<'_> {
        return Chunks {
            next: &self.head,
        };
    }

    #[inline]
    pub fn writer(&self) -> Writer {
        Writer { next: &self.head }
    }
}

pub struct Writer<'a> {
    next: &'a ChunkPromise,
}

// should this be a futures::Sink? don't need the async part
impl<'a> Writer<'a> {
    pub fn push(&mut self, buf: Bytes) {
        let next = SyncPromise::new();
        let chunk = Chunk { buf, next };
        let chunk = self.next.resolve(Some(Box::new(chunk))).unwrap();
        self.next = &chunk.as_ref().unwrap().next;
    }
}

impl<'a> Drop for Writer<'a> {
    fn drop(&mut self) {
        let _ = self.next.resolve(None);
    }
}

#[derive(Debug, Clone)]
pub struct Chunks<'a> {
    next: &'a SyncPromise<Option<Box<Chunk>>>,
    //next: SyncPromiseResult<'a, Option<Box<Chunk>>>,
}

impl<'a> Chunks<'a> {
    pub async fn next(&mut self) -> Option<Bytes> {
        let chunk = self.next.result().await.as_ref()?;
        self.next = &chunk.next;
        return Some(chunk.buf.clone());
    }
}

//impl<'a> Stream for Chunks<'a> {
//    type Item = &'a [u8];
//
//    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<Self::Item>> {
//        match Pin::new(&mut self.next).poll(cx) {
//            Poll::Ready(item) => {
//                let item = match item {
//                    Some(chunk) => {
//                        self.next = chunk.next.result();
//                        Some(&chunk.buf[..])
//                    }
//                    None => None,
//                };
//                Poll::Ready(item)
//            }
//            Poll::Pending => Poll::Pending,
//        }
//    }
//}

#[cfg(test)]
mod test {
    use super::*;
    use futures::{executor, task::SpawnExt, StreamExt};
    use std::sync::Arc;
    #[test]
    fn test_entry_chunks() {
        let entry = Arc::new(Entry {
            head: ChunkPromise::new(),
        });
        let consumer_entry = entry.clone();

        let mut pool = executor::LocalPool::new();
        let spawner = pool.spawner();

        spawner
            .spawn(async move {
                let mut head: &SyncPromise<_> = &entry.head;
                for i in 0..10 {
                    let chunk = Chunk {
                        buf: vec![i as u8],
                        next: SyncPromise::new(),
                    };
                    println!("{:?}", chunk);
                    let chunk = head.resolve(Some(Box::new(chunk))).unwrap();
                    head = &chunk.as_ref().unwrap().next;
                }

                head.resolve(None).unwrap();
            })
            .unwrap();

        for _ in 0..1 {
            let entry = consumer_entry.clone();
            spawner
                .spawn(async move {
                    let mut chunks = entry.chunks();
                    let mut counter = 0;
                    while let Some(chunk) = chunks.next().await {
                        let chunk: &[u8] = chunk;
                        println!("consume: {:?}", chunk);
                        assert_eq!(counter, chunk[0]);
                        counter += 1;
                    }
                    assert_eq!(counter, 10);
                })
                .unwrap();
        }

        pool.run();
    }
}
