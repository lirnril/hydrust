#![allow(incomplete_features)]
#![feature(str_split_once, generic_associated_types)]

use std::{
    fs,
    path::{Path, PathBuf},
    convert::Infallible,
    net::SocketAddr,
    sync::{atomic::{Ordering::*, AtomicU64}, Arc},
    time::Duration,
};

use clap::Clap;

use eyre::Result;

use hyper::{
    Body, Request, Response, Server, StatusCode,
    service::{make_service_fn, service_fn},
};

mod cache;
use cache::Cache;

mod backend;
use backend::{Backend, Entry};

mod body;

#[derive(Debug, Clap)]
struct Args {
    #[clap(short, long, default_value = "hydrust.toml")]
    config: PathBuf,
}


#[derive(Debug, Clone, serde::Deserialize)]
struct Config {
    #[serde(default = "default_listen_addr")]
    listen_addr: SocketAddr,
    #[serde(default = "default_header_backend_hint")]
    header_backend_hint: String,
}

impl Config {
    fn load<P: AsRef<Path>>(path: &P) -> Result<Config> {
        let file_contents = fs::read_to_string(&path)?;
        let config = toml::from_str(&file_contents)?;
        Ok(config)
    }
}

fn default_listen_addr() -> SocketAddr {
    SocketAddr::from(([127, 0, 0, 1], 3000))
}

fn default_header_backend_hint() -> String {
    "X-Backend-Hint".into()
}

struct App {
    cache: Cache<CacheBackend>,
    config: Config,
}

struct CacheBackend {
    backend: Box<dyn Backend+Send+Sync>,
}


#[derive(Debug)]
pub struct RequestInfo<'a> {
    upstream_path: &'a str,
    backend_hint: &'a str,
}

impl cache::Backend for CacheBackend {
    type Input<'i> = RequestInfo<'i>;
    type Key = (String, String);
    type Value = Arc<Entry>;

    fn get(&self, req: RequestInfo<'_>) -> Result<Arc<Entry>> {
        Ok(self.backend.get(req)?)
    }

    fn make_cache_key(&self, req: &RequestInfo) -> (String, String) {
        (req.upstream_path.to_string(), req.backend_hint.to_string())
    }

    fn validate(&self, entry: &Arc<Entry>) -> bool {
        if let Some(Err(_)) = entry.completed.get() {
            eprintln!("invalidating failed request");
            return false;
        }

        if let Some(parts) = entry.response.get() {
            let good = parts.status.is_success();
            let fresh = entry.time.elapsed() < Duration::from_secs(5);
            if !good && !fresh {
                eprintln!("invalidating old unsuccessful response: {:?}",
                    parts);
                return false;
            }
        }

        return true;
    }
}

impl App {
    fn process<'r>(&self, req: &'r Request<Body>) -> RequestInfo<'r> {
        let upstream_path = req.uri().path();
        let backend_hint = req.headers().get("X-Backend-Hint")
            .and_then(|v| v.to_str().ok())
            .unwrap_or("default");
        RequestInfo { upstream_path, backend_hint }
    }

    async fn handle(&self, req: Request<Body>)
            -> std::result::Result<Response<Body>, hyper::http::Error> {
        // top-level http handler, just forward and handle errors
        match self.actually_handle(req).await {
            Ok(response) => Ok(response),
            Err(e) => {
                eprintln!("{:?}", e);
                Response::builder()
                        .status(StatusCode::INTERNAL_SERVER_ERROR)
                        // don't tell the client what exactly went wrong
                        .body(Body::from("rip"))
            }
        }
    }

    async fn actually_handle(&self, req: Request<Body>)
            -> Result<Response<Body>> {
        let request_info = self.process(&req);
        let entry: Arc<Entry> = self.cache.get(request_info).await?;
        // stream it to the client
        let body = entry.to_hyper_body();
        let response = Response::new(body);
        Ok(response)
    }

    async fn listen_and_serve(self: &Arc<Self>) -> Result<()> {
        let counter = Arc::new(AtomicU64::new(0));
        let counter_ = counter.clone();

        tokio::spawn(async move {
            let mut interval = tokio::time::interval(Duration::from_secs(1));
            let mut prev = 0;
            loop {
                interval.tick().await;
                let n = counter.load(Relaxed);
                if n != prev {
                    eprintln!("counter: +{}", n - prev);
                }
                prev = n;
            }
        });

        let app = Arc::clone(self);
        // tokio boilerplate
        let make_service = make_service_fn(move |_conn| {
            let app = app.clone(); // gotta clone the arc before the async part
            let counter = counter_.clone();
            async move {
                let app = app.clone();
                let counter = counter.clone();
                Ok::<_, Infallible>(service_fn(move |req| {
                    let app: Arc<App> = app.clone();
                    let counter = counter.clone();
                    async move {
                        counter.fetch_add(1, Relaxed);
                        app.handle(req).await
                    }
                }))
            }
        });

        Server::bind(&self.config.listen_addr).serve(make_service).await?;


        Ok(())
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let args = Args::parse();
    let config = Config::load(&args.config)?;
    println!("{:#?}", config);

        let client = Arc::new(hyper::Client::new());
        //let final_ = backend::Final { client: client.clone() };
        //let sharding = backend::Sharding {
        //    upstreams: ["foo.localhost:9001", "bar.localhost:9001", "baz.localhost:9001"]
        //        .iter().map(|s| s.to_string()).collect(),
        //    client: client.clone(),
        //};
        //let mut backend = backend::Named::default();
        //backend.backends.insert("sharding".to_string(), Box::new(sharding));
        //backend.backends.insert("final".to_string(), Box::new(final_));
        let backend = backend::Sharding {
            upstreams: ["127.0.0.1:9001"]
                .iter().map(|s| s.to_string()).collect(),
            client: client.clone(),
        };
        let backend = Box::new(backend);
        let backend = CacheBackend { backend };
        let cache = Cache::new(backend);

    let app = Arc::new(App{ cache, config, });
    app.listen_and_serve().await?;

    Ok(())
}
