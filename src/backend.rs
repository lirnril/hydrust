use std::sync::Arc;
use std::collections::{BTreeMap, hash_map::DefaultHasher};
use std::hash::{Hash, Hasher};
use std::time::Instant;

use tokio::task;

use http::response::Parts;

use hyper::Uri;
use hyper::header;
use hyper::body::HttpBody;

use eyre::{eyre, bail, WrapErr};

use sync_promise::SyncPromise;

use super::Result;
use super::RequestInfo;
use crate::body;

type Client =
    hyper::client::Client<hyper::client::HttpConnector, hyper::body::Body>;

pub trait Backend {
    fn get(&self, req: RequestInfo) -> Result<Arc<Entry>>;
}

pub struct Final {
    pub client: Arc<Client>,
}

pub struct Sharding {
    pub upstreams: Vec<String>,
    pub client: Arc<Client>,
}

#[derive(Default)]
pub struct Named {
    pub backends: BTreeMap<String, Box<dyn Backend+Send+Sync>>,
}

fn final_url(upstream_path: &str) -> Result<Uri> {
    let path = upstream_path.strip_prefix("/")
        .ok_or_else(|| eyre!("path didn't start with `/`: {:?}",
            upstream_path))?;
    let (upstream, path) = path.split_once('/')
        .ok_or_else(|| eyre!("not enough path components: {:?}",
            upstream_path))?;
    if upstream.contains(|c: char| c != '_' && !c.is_ascii_digit()) {
        bail!("bad non-dotted-decimal upstream: {:?}", upstream_path);
    }
    //eprintln!("upstream: {:?}", upstream);
    let mut upstream_nums = upstream.split('_');
    let mut n = || upstream_nums.next().ok_or_else(
        || eyre!("not enough dotted decimals: {:?}", upstream_path));
    let url = format!("http://{}.{}.{}.{}/{}",
        n()?, n()?, n()?, n()?, path).parse()
        .wrap_err("couldn't format upstream url")?;
    Ok(url)
}

fn fetch_url(url: Uri, client: Arc<Client>) -> Result<Arc<Entry>> {
    let entry_read = Arc::new(Entry::new());
    let entry_write = entry_read.clone();
    task::spawn(async move {
        let result = fetch(&entry_write, &client, &url).await;
        if let Err(e) = &result {
            // how do we get it out of the cache? next fetch checks?
            eprintln!("couldn't fetch {}: {:#?}", url, e);
        }
        entry_write.completed.resolve(result).unwrap();
    });
    Ok(entry_read)
}

impl Backend for Final {
    fn get(&self, req: RequestInfo) -> Result<Arc<Entry>> {
        let url = final_url(req.upstream_path)?;
        fetch_url(url, self.client.clone())
    }
}

impl Backend for Sharding {
    fn get(&self, req: RequestInfo) -> Result<Arc<Entry>> {
        let i = match self.upstreams.len() {
            0 => bail!("no upstreams available"),
            1 => 0,
            _ => {
                // pretend this is some sort of rendezvous hash and we
                // actually put all the relevant fields in there
                let mut s = DefaultHasher::new();
                Hash::hash(req.upstream_path, &mut s);
                let i = s.finish() as usize;
                i % self.upstreams.len()
            }
        };
        let upstream = &self.upstreams[i];

        let url = format!("http://{}{}", upstream, req.upstream_path).parse()
            .wrap_err("couldn't format upstream url")?;
        //eprintln!("using upstream {}", upstream);
        fetch_url(url, self.client.clone())
    }
}

impl Backend for Named {
    fn get(&self, req: RequestInfo) -> Result<Arc<Entry>> {
        let backend = self.backends.get(req.backend_hint)
            .ok_or_else(|| eyre!("no backend for backend hint: {:?}",
                req.backend_hint))?;
        backend.get(req)
    }
}

#[derive(Debug)]
pub struct Entry {
    pub time: Instant,
    pub response: SyncPromise<Parts>,
    pub content_length: SyncPromise<Option<usize>>,
    body: body::Body, // not pub b/c you don't get to write to it
    pub completed: SyncPromise<eyre::Result<()>>,
}

impl Entry {
    //pub fn body(&self) -> body::Chunks { self.body.chunks() }

    pub fn new() -> Entry {
        Entry {
            time: Instant::now(),
            response: SyncPromise::new(),
            content_length: SyncPromise::new(),
            body: body::Body::default(),
            completed: SyncPromise::new(),
        }
    }

    pub fn to_hyper_body(self: &Arc<Self>) -> hyper::Body {
        let (mut sender, body) = hyper::body::Body::channel();

        let entry = self.clone();

        task::spawn(async move {
            // TODO: select on timeout
            let mut chunks = entry.body.chunks();
            while let Some(chunk) = chunks.next().await {
                let result = sender.send_data(chunk).await;
                if let Err(e) = result {
                    // TODO: think about error handling better
                    eprintln!("couldn't stream body to client: {:#?}", e);
                    sender.abort();
                    return;
                }
            }

            let result = entry.completed.result().await;
            if result.is_err() {
                sender.abort();
            }
        });

        return body;
    }
}

async fn fetch(entry: &Entry, client: &Client, url: &Uri) -> Result<()> {
    // this is kinda ugly: writer needs to "close" body on drop in case of
    // any error
    let mut writer = entry.body.writer();
    let resp = client.get(url.clone()).await?;
    let (parts, mut body) = resp.into_parts();
    let content_length = parts.headers.get(header::CONTENT_LENGTH)
        .and_then(|v| v.to_str().ok())
        .and_then(|s| s.parse().ok());
    entry.content_length.resolve(content_length).unwrap();
    entry.response.resolve(parts).unwrap();
    // TODO: control how much we read
    while let Some(data) = body.data().await {
        writer.push(data?);
    }

    Ok(())
}
