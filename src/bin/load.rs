use std::sync::{Arc, atomic::{AtomicU64, Ordering}};
use std::time::Duration;

use tokio::time::interval;

use hyper::{Uri, body::HttpBody};

use eyre::{Result, bail};

type Client =
    hyper::client::Client<hyper::client::HttpConnector, hyper::body::Body>;

#[tokio::main(worker_threads = 16)]
async fn main() {
    let counter = Arc::new(AtomicU64::new(0));
    let client = Arc::new(hyper::Client::new());

    for _ in 0..512 {
        let counter = counter.clone();
        let client = client.clone();
        tokio::spawn(async move {
            let n = rand::random::<u32>() % 1024;
            loop {
                let url: Uri =
                    format!("http://localhost:3000/video/file-{:04}", n)
                        .parse().unwrap();
                if let Err(e) = make_request(&client, &url).await {
                    eprintln!("error: {:?}", e);
                } else {
                    counter.fetch_add(1, Ordering::Relaxed);
                }
            }
        });
    }

    let mut interval = interval(Duration::from_secs(1));
    let mut prev = 0;
    loop {
        interval.tick().await;
        let n = counter.load(Ordering::Relaxed);
        if n != prev {
            eprintln!("counter: +{}", n - prev);
        }
        prev = n;
    }
}

async fn make_request(client: &Client, url: &Uri) -> Result<()> {
    let resp = client.get(url.clone()).await?;
    if !resp.status().is_success() {
        bail!("bad status: {:?}", resp);
    }

    let mut body = resp.into_body();
    while let Some(_) = body.data().await {}

    Ok(())
}
