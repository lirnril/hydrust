use std::{
    fmt,
    hash,
    collections::{HashMap, BTreeMap},
};

use tokio::{
    sync::Mutex,
};

use super::Result;

pub struct Cache<B: Backend> {
    pub backend: B,
    max_entries: usize,
    // TODO: this is proobably better as an rwlock but then we don't get to
    // use the cool hashmap entry api anymore
    lru: Mutex<Lru<B::Key, B::Value>>,
}

struct Lru<K, V> {
    entries: HashMap<K, (u64, V)>,
    recencies: BTreeMap<u64, K>,
}

pub trait Backend {
    type Input<'a>;
    type Key: Clone + Eq + hash::Hash + fmt::Debug;
    type Value: Clone;

    fn get<'a>(&self, key: Self::Input<'a>) -> Result<Self::Value>;

    fn make_cache_key<'a>(&self, input: &Self::Input<'a>) -> Self::Key;

    fn validate(&self, _: &Self::Value) -> bool { return true; }
}

impl<B: Backend> Cache<B> {
    pub fn new(backend: B) -> Self {
        Cache {
            backend: backend,
            lru: Mutex::new(Lru {
                entries: HashMap::new(),
                recencies: BTreeMap::new(),
            }),
            max_entries: 1024*1024,
        }
    }

    pub async fn get<'a>(&self, input: <B as Backend>::Input<'a>)
            -> Result<<B as Backend>::Value> {
        use std::collections::hash_map::Entry;
        let mut lru = self.lru.lock().await;
        let Lru { ref mut entries, ref mut recencies } = &mut *lru;
        let key = self.backend.make_cache_key(&input);
        let n = entries.len();
        let entry = match entries.entry(key) {
            // for hydrust, this clones an Arc for an Entry
            Entry::Occupied(mut e) => {
                //eprintln!("cache hit for {:?}", e.key());
                let (r, entry) = e.get_mut();
                if !self.backend.validate(entry) {
                    eprintln!("cached value is bad, retrying");
                    *entry = self.backend.get(input)?;
                }
                let max_r = *recencies.keys().next_back().unwrap();
                if *r != max_r {
                    let key = recencies.remove(r).unwrap();
                    *r = max_r + 1;
                    recencies.insert(*r, key);
                }
                entry.clone()
            }
            Entry::Vacant(e) => {
                eprintln!("cache miss for {:?} ({} cached)", e.key(), n);
                let key = e.key();
                let entry = self.backend.get(input)?;
                let r = recencies.keys().next_back()
                    .map(|&v| v + 1).unwrap_or(0);
                recencies.insert(r, key.clone());
                e.insert((r, entry)).1.clone()
            }
        };

        while entries.len() > self.max_entries {
            let r = *recencies.keys().next().unwrap();
            let key = recencies.remove(&r).unwrap();
            entries.remove(&key).unwrap();
        }

        Ok(entry)

        //Ok(<B as Backend>::get(&self.backend, &key).await?)
    }
}
