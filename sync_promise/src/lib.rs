use std::{
    cell::UnsafeCell,
    fmt::{self, Debug},
    future::Future,
    pin::Pin,
    sync::{
        atomic::{AtomicU8, Ordering},
        Mutex,
    },
    task::{Context, Poll, Waker},
};

use slotmap::{DefaultKey, SlotMap};

pub struct SyncPromise<T> {
    data: UnsafeCell<Option<T>>,
    wakers: Mutex<SlotMap<DefaultKey, Waker>>,
    state: AtomicU8, // PromiseState
}

#[repr(u8)]
#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone, Copy, Hash)]
enum PromiseState {
    UNRESOLVED,
    RESOLVING,
    RESOLVED,
}

// safety: we're careful about mutating the UnsafeCell
unsafe impl<T> Sync for SyncPromise<T> {}

impl<T> Debug for SyncPromise<T> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let resolved = self.resolved();
        write!(fmt, "SyncPromise {{ resolved: {}, .. }}", resolved)
    }
}

impl<T> Default for SyncPromise<T> {
    fn default() -> SyncPromise<T> { SyncPromise::new() }
}

impl<T> SyncPromise<T> {
    pub fn new() -> Self {
        SyncPromise {
            data: UnsafeCell::new(None),
            wakers: Mutex::new(SlotMap::new()),
            state: AtomicU8::new(PromiseState::UNRESOLVED as u8),
        }
    }

    pub fn resolve(&self, data: T) -> Result<&T, T> {
        use PromiseState::*;
        let mut wakers = self.wakers.lock().unwrap();

        let prev = self.state.swap(RESOLVING as u8, Ordering::SeqCst);
        if prev != UNRESOLVED as u8 {
            return Err(data);
        }

        let mut data = Some(data);
        let ptr = self.data.get();
        unsafe {
            // Safety: no one should be looking at `self.data` until we
            // toggle `state` to `RESOLVED`.
            // This is the only time `self.data` gets mutated.
            ptr.swap(&mut data);
        }

        let prev = self.state.swap(RESOLVED as u8, Ordering::SeqCst);
        if prev != RESOLVING as u8 {
            // this should not be possible
            panic!("unexpected race completing sync promise");
        }

        for waker in wakers.drain() {
            waker.1.wake();
        }

        // Safety: we just stored the value so we're good, we won't
        // mutate it ever again so immutable reference is ok, the
        // lifetime of the value matches that of `self`.
        Ok(unsafe { (*ptr).as_ref().unwrap() })
    }

    #[inline]
    pub fn result(&self) -> SyncPromiseResult<T> {
        SyncPromiseResult {
            inner: &self,
            slot: None,
        }
    }

    #[inline]
    pub fn resolved(&self) -> bool {
        let state = self.state.load(Ordering::SeqCst);
        state == PromiseState::RESOLVED as u8
    }

    #[inline]
    pub fn get(&self) -> Option<&T> {
        if self.resolved() {
            // Safety: `resolved` promises that `data` has been set and
            // will not be mutated again. The returned reference has the
            // lifetime of our reference to the `SyncPromise`, so it doesn't
            // outlive the promise.
            unsafe { self.get_unchecked() }
        } else {
            None
        }
    }

    // don't call concurrently with resolve()
    #[inline]
    unsafe fn get_unchecked(&self) -> Option<&T> {
        let ptr = self.data.get();
        // Safety: ok as long as `resolve` won't be mutating it. Lifetime
        // of the returned reference matches that of `self`.
        (*ptr).as_ref()
    }
}

#[derive(Debug)]
pub struct SyncPromiseResult<'s, T> {
    inner: &'s SyncPromise<T>,
    slot: Option<DefaultKey>,
}

impl<'a, T> Future for SyncPromiseResult<'a, T> {
    type Output = &'a T;

    fn poll<'b>(mut self: Pin<&mut Self>, cx: &'b mut Context<'_>) -> Poll<&'a T> {
        if let Some(data) = self.inner.get() {
            self.remove_waker();
            return Poll::Ready(data);
        }

        let mut wakers = self.inner.wakers.lock().unwrap();
        // check again to be sure we don't miss the promise being resolved
        // between the previous check and taking the lock
        if let Some(data) = self.inner.get() {
            drop(wakers);
            self.remove_waker();
            return Poll::Ready(data);
        }

        // register our waker, or overwrite previously registered waker
        let waker = cx.waker().clone();
        match self.slot {
            Some(slot) => *wakers.get_mut(slot).unwrap() = waker,
            None => self.slot = Some(wakers.insert(waker)),
        }

        Poll::Pending
    }
}

impl<'a, T> SyncPromiseResult<'a, T> {
    fn remove_waker(&mut self) {
        let slot = match self.slot.take() {
            Some(slot) => slot,
            None => return,
        };

        let mut wakers = match self.inner.wakers.lock() {
            Ok(lock) => lock,
            Err(_) => return, // w/e
        };

        wakers.remove(slot);
    }
}

impl<'a, T> Clone for SyncPromiseResult<'a, T> {
    fn clone(&self) -> Self {
        SyncPromiseResult {
            inner: self.inner,
            slot: None,
        }
    }
}

impl<'a, T> Drop for SyncPromiseResult<'a, T> {
    fn drop(&mut self) {
        // TODO: is this worth locking the mutex on drop?
        self.remove_waker();
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use futures::{executor, task::SpawnExt, FutureExt};
    use std::sync::Arc;

    #[test]
    fn test_basic() {
        let p = SyncPromise::<String>::new();
        assert!(unsafe { p.get_unchecked().is_none() });
        let r = p.result();
        assert!(r.now_or_never().is_none());
        p.resolve("yep".to_string()).unwrap();
        executor::block_on(async move {
            let r = p.result();
            r.await; // todo: timeout probably
            let r = p.result();
            assert_eq!(*r.await, "yep");
        });
    }

    #[test]
    fn test_recursive() {
        type Promise = SyncPromise<Option<Box<Chunk>>>;
        #[derive(Debug)]
        struct Chunk {
            buf: Vec<u8>,
            next: Promise,
        }

        let head = Arc::new(Promise::new());
        let consumer_head = head.clone();

        let mut pool = executor::LocalPool::new();
        let spawner = pool.spawner();

        spawner
            .spawn(async move {
                let mut head: &SyncPromise<_> = &head;
                for i in 0..10 {
                    let chunk = Chunk {
                        buf: vec![i as u8],
                        next: SyncPromise::new(),
                    };
                    println!("{:?}", chunk);
                    let chunk = head.resolve(Some(Box::new(chunk))).unwrap();
                    head = &chunk.as_ref().unwrap().next;
                }

                head.resolve(None).unwrap();
            })
            .unwrap();

        for _ in 0..10 {
            let head = consumer_head.clone();
            spawner
                .spawn(async move {
                    let mut head: &SyncPromise<_> = &head;
                    let mut counter = 0;
                    loop {
                        let r: &Option<Box<Chunk>> = head.result().await;
                        if r.is_none() {
                            break;
                        }
                        let chunk = r.as_ref().unwrap();
                        println!("consume: {:?}", chunk);
                        assert_eq!(counter, chunk.buf[0]);
                        counter += 1;
                        head = &chunk.next;
                    }
                    assert_eq!(counter, 10);
                })
                .unwrap();
        }

        pool.run();
    }
}
